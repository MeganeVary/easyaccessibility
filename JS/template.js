var elem = document.querySelector('body');

var valueTemplate = elem.getAttribute("data-template");


let buttonCloseMenuAccessible = document.getElementById("closeMenu");
let menuAccessible = document.getElementById("menuAccessible");

let btnModifiedPrimary = document.getElementsByClassName("btnModified");
let btnModifiedSecondary = document.getElementsByClassName("informationsButtonSecondary");

let containerSousCategory = document.getElementsByClassName('containerSousCategory')
let tittleMenuAccessible = document.getElementsByClassName('tittleMenuAccessible')


if ((valueTemplate == "premiumGoldenDark")) {
  menuAccessible.style.backgroundColor = "var(--backgroundMenu_templatePremiumGoldenDark)";
  buttonCloseMenuAccessible.style.backgroundColor = "var(--backgroundSecondaryColor_templatePremiumGoldenDark)";
  buttonMenuAccessible.style.backgroundColor = "var(--backgroundMenu_templatePremiumGoldenDark)";
  buttonMenuAccessible.style.color = "white";

  let links = document.getElementsByClassName("linkshighlightActive");
  var index = 0;
  var indexB = 0;
  var indexC = 0;
  var indexD = 0;
  var indexE = 0;

  let linksLength = links.length;
  let btnModifiedPrimaryLength = btnModifiedPrimary.length;
  let btnModifiedSecondaryLength = btnModifiedSecondary.length;
  let containerSousCategoryLength = containerSousCategory.length;
  let tittleMenuAccessibleLength = tittleMenuAccessible.length;

  for (; index < btnModifiedPrimaryLength; index++) {
    btnModifiedPrimary[index].style.backgroundColor = "var(--backgroundSecondaryColor_templatePremiumGoldenDark)";
  }
  for (; indexB < containerSousCategoryLength; indexB++) {
    containerSousCategory[indexB].style.backgroundColor = "var(--backgroundTertiaryCorlor_templatePremiumGoldenDark)";
  }

  for (; indexC < linksLength; indexC++) {
    links[indexC].style.border = "2px solid var(--link_templatePremium)"; //todo link plus titre
  }

  for (; indexD < btnModifiedSecondaryLength; indexD++) {
    btnModifiedSecondary[indexD].style.backgroundColor = "var(--backgroundSecondaryColor_templatePremiumGoldenDark)";
    btnModifiedSecondary[indexD].style.color = "var(--backgroundMenu_templatePremiumGoldenDark)";
  }
  for (; indexE < tittleMenuAccessibleLength; indexE++) {
    tittleMenuAccessible[indexE].style.color = "var(--backgroundPrimaryColor_templatePremiumGoldenDark)";
  }
}

if ((valueTemplate == "darkBlackPink")) {
  menuAccessible.style.backgroundColor = "var(--backgroundMenu_templateBlackPink)";
  buttonCloseMenuAccessible.style.backgroundColor = "var(--backgroundSecondaryColor_templateBlackPink)";
  buttonMenuAccessible.style.backgroundColor = "var(--backgroundMenu_templatePremiumGoldenDark)";
  buttonMenuAccessible.style.color = "white";

  let links = document.getElementsByClassName("linkshighlightActive");
  var index = 0;
  var indexB = 0;
  var indexC = 0;
  var indexD = 0;
  var indexE = 0;

  let linksLength = links.length;
  let btnModifiedPrimaryLength = btnModifiedPrimary.length;
  let btnModifiedSecondaryLength = btnModifiedSecondary.length;
  let containerSousCategoryLength = containerSousCategory.length;
  let tittleMenuAccessibleLength = tittleMenuAccessible.length;

  for (; index < btnModifiedPrimaryLength; index++) {
    btnModifiedPrimary[index].style.backgroundColor = "var(--backgroundSecondaryColor_templateBlackPink)";
  }
  for (; indexB < containerSousCategoryLength; indexB++) {
    containerSousCategory[indexB].style.backgroundColor = "var(--backgroundPrimaryColor_templateBlackPink)";
  }

  for (; indexC < linksLength; indexC++) {
    links[indexC].style.border = "2px solid var(--link_templatePremium)"; //todo link plus titre
  }

  for (; indexD < btnModifiedSecondaryLength; indexD++) {
    btnModifiedSecondary[indexD].style.backgroundColor = "var(--backgroundSecondaryColor_templateBlackPink)";
    btnModifiedSecondary[indexD].style.color = "var(--backgroundMenu_templatePremiumGoldenDark)";
  }
  for (; indexE < tittleMenuAccessibleLength; indexE++) {
    tittleMenuAccessible[indexE].style.color = "var(--backgroundPrimaryColor_templateBlackPink)";
  }
}

if ((valueTemplate == "lightBlackPink1")) {
  menuAccessible.style.backgroundColor = "var(--backgroundMenu_templateBlackPink)";
  buttonCloseMenuAccessible.style.backgroundColor = "var(--backgroundSecondaryColor_templateBlackPink)";
  buttonMenuAccessible.style.backgroundColor = "var(--backgroundMenu_templatePremiumGoldenDark)";
  buttonMenuAccessible.style.color = "white";

  let links = document.getElementsByClassName("linkshighlightActive");
  var index = 0;
  var indexB = 0;
  var indexC = 0;
  var indexD = 0;
  var indexE = 0;

  let linksLength = links.length;
  let btnModifiedPrimaryLength = btnModifiedPrimary.length;
  let btnModifiedSecondaryLength = btnModifiedSecondary.length;
  let containerSousCategoryLength = containerSousCategory.length;
  let tittleMenuAccessibleLength = tittleMenuAccessible.length;

  for (; index < btnModifiedPrimaryLength; index++) {
    btnModifiedPrimary[index].style.backgroundColor = "var(--backgroundMenu_templateBlackPink)";
  }
  for (; indexB < containerSousCategoryLength; indexB++) {
    containerSousCategory[indexB].style.backgroundColor = "var(--backgroundSecondaryColor_templateBlackPink)";
  }

  for (; indexC < linksLength; indexC++) {
    links[indexC].style.border = "2px solid var(--link_templatePremium)"; //todo link plus titre
  }

  for (; indexD < btnModifiedSecondaryLength; indexD++) {
    btnModifiedSecondary[indexD].style.backgroundColor = "var(--backgroundSecondaryColor_templateBlackPink)";
    btnModifiedSecondary[indexD].style.color = "var(--backgroundMenu_templatePremiumGoldenDark)";
  }
  for (; indexE < tittleMenuAccessibleLength; indexE++) {
    tittleMenuAccessible[indexE].style.color = "var(--backgroundPrimaryColor_templateBlackPink)";
  }
}

if ((valueTemplate == "lightBlackPink")) {
  menuAccessible.style.backgroundColor = "var(--backgroundSecondaryColor_templateBlackPink)";
  buttonCloseMenuAccessible.style.backgroundColor = "var(--backgroundMenu_templateBlackPink)";
  buttonMenuAccessible.style.backgroundColor = "var(--backgroundMenu_templatePremiumGoldenDark)";
  buttonMenuAccessible.style.color = "white";

  let links = document.getElementsByClassName("linkshighlightActive");
  var index = 0;
  var indexB = 0;
  var indexC = 0;
  var indexD = 0;
  var indexE = 0;
  var indexF = 0;


  let linksLength = links.length;
  let btnModifiedPrimaryLength = btnModifiedPrimary.length;
  let btnModifiedSecondaryLength = btnModifiedSecondary.length;
  let containerSousCategoryLength = containerSousCategory.length;
  let tittleMenuAccessibleLength = tittleMenuAccessible.length;

  for (; index < btnModifiedPrimaryLength; index++) {
    btnModifiedPrimary[index].style.backgroundColor = "var(--backgroundMenu_templateBlackPink)";
  }
  for (; indexB < containerSousCategoryLength; indexB++) {
    containerSousCategory[indexB].style.backgroundColor = "var(--backgroundTertiaryCorlor_templateBlackPink)";
    // containerSousCategory[indexB].style.style.boxShadow = "0px 2px 2px 0px rgba(0, 0, 0, 0.1)";
  }
  for (; indexF < containerSousCategoryLength; indexF++) {
    containerSousCategory[indexF].style["boxShadow"]= " 4px 4px 4px 0px rgba(0, 0, 0, 0.1)";
  }
  for (; indexC < linksLength; indexC++) {
    links[indexC].style.border = "2px solid var(--link_templatePremium)"; //todo link plus titre
  }

  for (; indexD < btnModifiedSecondaryLength; indexD++) {
    btnModifiedSecondary[indexD].style.backgroundColor = "var(--backgroundPrimaryColor_templateBlackPink)";
    btnModifiedSecondary[indexD].style.color = "var(--backgroundTertiaryCorlor_templateBlackPink)";
  }
  for (; indexE < tittleMenuAccessibleLength; indexE++) {
    tittleMenuAccessible[indexE].style.color = "var(--backgroundPrimaryColor_templateBlackPink)";
  }
}


if ((valueTemplate == "mrGrey")) {
  menuAccessible.style.backgroundColor = "var(--backgroundMenu_templateMrGrey)";
  buttonCloseMenuAccessible.style.backgroundColor = "var(--backgroundSecondaryColor_templateMrGrey)";
  buttonMenuAccessible.style.backgroundColor = "var(--backgroundMenu_templatePremiumGoldenDark)";
  buttonMenuAccessible.style.color = "white";

  let links = document.getElementsByClassName("linkshighlightActive");
  var index = 0;
  var indexB = 0;
  var indexC = 0;
  var indexD = 0;
  var indexE = 0;
  var indexF = 0;


  let linksLength = links.length;
  let btnModifiedPrimaryLength = btnModifiedPrimary.length;
  let btnModifiedSecondaryLength = btnModifiedSecondary.length;
  let containerSousCategoryLength = containerSousCategory.length;
  let tittleMenuAccessibleLength = tittleMenuAccessible.length;

  for (; index < btnModifiedPrimaryLength; index++) {
    btnModifiedPrimary[index].style.backgroundColor = "var(--backgroundSecondaryColor_templateMrGrey)";
    btnModifiedPrimary[index].style.color = "var(--backgroundTertiaryCorlor_templateMrGrey)";
  }
  for (; indexB < containerSousCategoryLength; indexB++) {
    containerSousCategory[indexB].style.backgroundColor = "var(--backgroundPrimaryColor_templateMrGrey)";
  }
  for (; indexF < containerSousCategoryLength; indexF++) {
    containerSousCategory[indexF].style["boxShadow"]= " 4px 4px 4px 0px rgba(0, 0, 0, 0.1)";
  }
  for (; indexC < linksLength; indexC++) {
    links[indexC].style.border = "2px solid var(--link_templatePremium)"; //todo link plus titre
  }

  for (; indexD < btnModifiedSecondaryLength; indexD++) {
    btnModifiedSecondary[indexD].style.backgroundColor = "var(--backgroundPrimaryColor_templateMrGrey)";
    btnModifiedSecondary[indexD].style.color = "var(--backgroundTertiaryCorlor_templateMrGrey)";
  }
  for (; indexE < tittleMenuAccessibleLength; indexE++) {
    tittleMenuAccessible[indexE].style.color = "var(--backgroundPrimaryColor_templateBlackPink)";
  }
}

if ((valueTemplate == "bluDaBaDee")) {
  menuAccessible.style.backgroundColor = "var(--backgroundSecondaryColor_templateBluDaBaDee)";
  buttonCloseMenuAccessible.style.backgroundColor = "var(--backgroundMenu_templateBluDaBaDee)";
  buttonMenuAccessible.style.backgroundColor = "var(--backgroundMenu_templatePremiumGoldenDark)";
  buttonMenuAccessible.style.color = "white";

  let links = document.getElementsByClassName("linkshighlightActive");
  var index = 0;
  var indexB = 0;
  var indexC = 0;
  var indexD = 0;
  var indexE = 0;
  var indexF = 0;

  let linksLength = links.length;
  let btnModifiedPrimaryLength = btnModifiedPrimary.length;
  let btnModifiedSecondaryLength = btnModifiedSecondary.length;
  let containerSousCategoryLength = containerSousCategory.length;
  let tittleMenuAccessibleLength = tittleMenuAccessible.length;

  for (; index < btnModifiedPrimaryLength; index++) {
    btnModifiedPrimary[index].style.backgroundColor = "var(--backgroundPrimaryColor_templateBluDaBaDee)";
    btnModifiedPrimary[index].style.color = "var(--backgroundSecondaryColor_templateBluDaBaDee)";
  }
  for (; indexB < containerSousCategoryLength; indexB++) {
    containerSousCategory[indexB].style.backgroundColor = "var(--backgroundMenu_templateBluDaBaDee)";
  }
  for (; indexF < containerSousCategoryLength; indexF++) {
    containerSousCategory[indexF].style["boxShadow"]= " 4px 4px 4px 0px rgba(0, 0, 0, 0.1)";
  }
  for (; indexC < linksLength; indexC++) {
    links[indexC].style.border = "2px solid var(--link_templatePremium)"; //todo link plus titre
  }

  for (; indexD < btnModifiedSecondaryLength; indexD++) {
    btnModifiedSecondary[indexD].style.backgroundColor = "var(--backgroundTertiaryCorlor_templateBluDaBaDee)";
    btnModifiedSecondary[indexD].style.color = "var(--backgroundMenu_templateBluDaBaDee)";
  }
  for (; indexE < tittleMenuAccessibleLength; indexE++) {
    tittleMenuAccessible[indexE].style.color = "var(--backgroundPrimaryColor_templateBlackPink)";
  }
}

if ((valueTemplate == "basicBlue")) {
  menuAccessible.style.backgroundColor = "var(--backgroundMenu_templateBasicBlue)";
  buttonCloseMenuAccessible.style.backgroundColor = "var(--backgroundPrimaryColor_templateBasicBlue)";
  buttonMenuAccessible.style.backgroundColor = "var(--backgroundMenu_templatePremiumGoldenDark)";
  buttonMenuAccessible.style.color = "white";

  let links = document.getElementsByClassName("linkshighlightActive");
  var index = 0;
  var indexB = 0;
  var indexC = 0;
  var indexD = 0;
  var indexE = 0;
  var indexF = 0;

  let linksLength = links.length;
  let btnModifiedPrimaryLength = btnModifiedPrimary.length;
  let btnModifiedSecondaryLength = btnModifiedSecondary.length;
  let containerSousCategoryLength = containerSousCategory.length;
  let tittleMenuAccessibleLength = tittleMenuAccessible.length;

  for (; index < btnModifiedPrimaryLength; index++) {
    btnModifiedPrimary[index].style.backgroundColor = "var(--backgroundPrimaryColor_templateBasicBlue)";
    btnModifiedPrimary[index].style.color = "var(--backgroundMenu_templateBasicBlue)";
  }
  for (; indexB < containerSousCategoryLength; indexB++) {
    containerSousCategory[indexB].style.backgroundColor = "var(--backgroundMenu_templateBasicBlue)";
  }
  for (; indexF < containerSousCategoryLength; indexF++) {
    containerSousCategory[indexF].style["boxShadow"]= " 5px 5px 5px 0px rgba(0, 0, 0, 0.1)";
    containerSousCategory[indexF].style.border="1px solid #979797"
  }
  for (; indexC < linksLength; indexC++) {
    links[indexC].style.border = "2px solid var(--link_templatePremium)"; //todo link plus titre
  }

  for (; indexD < btnModifiedSecondaryLength; indexD++) {
    btnModifiedSecondary[indexD].style.backgroundColor = "var(--backgroundPrimaryColor_templateBasicBlue)";
    btnModifiedSecondary[indexD].style.color = "var(--backgroundMenu_templateBasicBlue)";
  }
  for (; indexE < tittleMenuAccessibleLength; indexE++) {
    tittleMenuAccessible[indexE].style.color = "var(--backgroundPrimaryColor_templateBlackPink)";
  }
}

if ((valueTemplate == "orangeBlue")) {
  menuAccessible.style.backgroundColor = "var(--backgroundMenu_templateOrangeBlue)";
  buttonCloseMenuAccessible.style.backgroundColor = "var(--backgroundMenu_templateOrangeBlue)";
  buttonMenuAccessible.style.backgroundColor = "var(--backgroundMenu_templatePremiumGoldenDark)";
  buttonMenuAccessible.style.color = "white";

  let links = document.getElementsByClassName("linkshighlightActive");
  var index = 0;
  var indexB = 0;
  var indexC = 0;
  var indexD = 0;
  var indexE = 0;
  var indexF = 0;

  let linksLength = links.length;
  let btnModifiedPrimaryLength = btnModifiedPrimary.length;
  let btnModifiedSecondaryLength = btnModifiedSecondary.length;
  let containerSousCategoryLength = containerSousCategory.length;
  let tittleMenuAccessibleLength = tittleMenuAccessible.length;

  for (; index < btnModifiedPrimaryLength; index++) {
    btnModifiedPrimary[index].style.backgroundColor = "var(--backgroundSecondaryColor_templateOrangeBluee)";
    btnModifiedPrimary[index].style.color = "var(--backgroundPrimaryColor_templateOrangeBluee)";
  }
  for (; indexB < containerSousCategoryLength; indexB++) {
    containerSousCategory[indexB].style.backgroundColor = "var(--backgroundMenu_templateBasicBlue)";
  }
  for (; indexF < containerSousCategoryLength; indexF++) {
    containerSousCategory[indexF].style["boxShadow"]= " 5px 5px 5px 0px rgba(0, 0, 0, 0.1)";
  }
  for (; indexC < linksLength; indexC++) {
    links[indexC].style.border = "2px solid var(--link_templatePremium)"; //todo link plus titre
  }

  for (; indexD < btnModifiedSecondaryLength; indexD++) {
    btnModifiedSecondary[indexD].style.backgroundColor = "var(--backgroundPrimaryColor_templateOrangeBluee)";
    btnModifiedSecondary[indexD].style.color = "var(--backgroundSecondaryColor_templateOrangeBluee)";
  }
  for (; indexE < tittleMenuAccessibleLength; indexE++) {
    tittleMenuAccessible[indexE].style.color = "var(--backgroundPrimaryColor_templateBlackPink)";
  }
}

if ((valueTemplate == "juzoSuzuya")) {
  menuAccessible.style.backgroundColor = "var(--backgroundMenu_templateJuzoSuzuya)";
  buttonCloseMenuAccessible.style.backgroundColor = "var(--backgroundMenu_templateOrangeBlue)";
  buttonMenuAccessible.style.backgroundColor = "var(--backgroundMenu_templatePremiumGoldenDark)";
  buttonMenuAccessible.style.color = "white";

  let links = document.getElementsByClassName("linkshighlightActive");
  var index = 0;
  var indexB = 0;
  var indexC = 0;
  var indexD = 0;
  var indexE = 0;
  var indexF = 0;

  let linksLength = links.length;
  let btnModifiedPrimaryLength = btnModifiedPrimary.length;
  let btnModifiedSecondaryLength = btnModifiedSecondary.length;
  let containerSousCategoryLength = containerSousCategory.length;
  let tittleMenuAccessibleLength = tittleMenuAccessible.length;

  for (; index < btnModifiedPrimaryLength; index++) {
    btnModifiedPrimary[index].style.backgroundColor = "var(--backgroundPrimaryColor_templateJuzoSuzuya)";
    btnModifiedPrimary[index].style.color = "var(--backgroundMenu_templateJuzoSuzuya)";
  }
  for (; indexB < containerSousCategoryLength; indexB++) {
    containerSousCategory[indexB].style.backgroundColor = "var(--backgroundSecondaryColor_templateJuzoSuzuya)";
  }
  for (; indexF < containerSousCategoryLength; indexF++) {
    containerSousCategory[indexF].style["boxShadow"]= " 5px 5px 5px 0px rgba(0, 0, 0, 0.1)";
  }
  for (; indexC < linksLength; indexC++) {
    links[indexC].style.border = "2px solid var(--link_templatePremium)"; //todo link plus titre
  }

  for (; indexD < btnModifiedSecondaryLength; indexD++) {
    btnModifiedSecondary[indexD].style.backgroundColor = "var(--backgroundTertiaryCorlor_templateJuzoSuzuya)";
    btnModifiedSecondary[indexD].style.color = "var(--backgroundMenu_templateJuzoSuzuya)";
  }
  for (; indexE < tittleMenuAccessibleLength; indexE++) {
    tittleMenuAccessible[indexE].style.color = "var(--backgroundPrimaryColor_templateBlackPink)";
  }
}

if ((valueTemplate == "fujiwara")) {
  menuAccessible.style.backgroundColor = "var(--backgroundSecondaryColor_templateBlackPink)";
  buttonCloseMenuAccessible.style.backgroundColor = "var(--backgroundTertiaryCorlor_templateFujiwara)";
  buttonMenuAccessible.style.backgroundColor = "var(--backgroundMenu_templatePremiumGoldenDark)";
  buttonMenuAccessible.style.color = "white";

  let links = document.getElementsByClassName("linkshighlightActive");
  var index = 0;
  var indexB = 0;
  var indexC = 0;
  var indexD = 0;
  var indexE = 0;
  var indexF = 0;


  let linksLength = links.length;
  let btnModifiedPrimaryLength = btnModifiedPrimary.length;
  let btnModifiedSecondaryLength = btnModifiedSecondary.length;
  let containerSousCategoryLength = containerSousCategory.length;
  let tittleMenuAccessibleLength = tittleMenuAccessible.length;

  for (; index < btnModifiedPrimaryLength; index++) {
    btnModifiedPrimary[index].style.backgroundColor = "var(--backgroundMenu_templateBlackPink)";
  }
  for (; indexB < containerSousCategoryLength; indexB++) {
    containerSousCategory[indexB].style.backgroundColor = "var(--backgroundTertiaryCorlor_templateBlackPink)";
    // containerSousCategory[indexB].style.style.boxShadow = "0px 2px 2px 0px rgba(0, 0, 0, 0.1)";
  }
  for (; indexF < containerSousCategoryLength; indexF++) {
    containerSousCategory[indexF].style["boxShadow"]= " 4px 4px 4px 0px rgba(0, 0, 0, 0.1)";
  }
  for (; indexC < linksLength; indexC++) {
    links[indexC].style.border = "2px solid var(--link_templatePremium)"; //todo link plus titre
  }

  for (; indexD < btnModifiedSecondaryLength; indexD++) {
    btnModifiedSecondary[indexD].style.backgroundColor = "var(--backgroundTertiaryCorlor_templateFujiwara)";
    btnModifiedSecondary[indexD].style.color = "var(--backgroundPrimaryColor_templateFujiwara)";
  }
  for (; indexE < tittleMenuAccessibleLength; indexE++) {
    tittleMenuAccessible[indexE].style.color = "var(--backgroundPrimaryColor_templateBlackPink)";
  }
}



